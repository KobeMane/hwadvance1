import { Employee } from "./Employee.js";

export class Programmer extends Employee {

  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  set salary(salary) {
    this._salary = salary;
  }

}
