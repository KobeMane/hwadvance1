import { Programmer } from "./Programmer.js";

const vladosProgramer = new Programmer("Vlad", 23, 270000, "eng");
vladosProgramer.salary = 12000;
console.log(vladosProgramer);

const dimasProgrammer = new Programmer("Dima", 21, 20000, "fr");
console.log(dimasProgrammer);

const dyshNilaProgrammer = new Programmer("Dysh", 33, 90000, "esp");
console.log(dyshNilaProgrammer);
